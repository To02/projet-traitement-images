# Projet traitement images


**Résumé**

Cette étude vise à traiter des images dans un mois entier.

Le traitement consiste à sélectionner l’image de référence pour chacun des quarts d’heures de la vidéo.

L’image de référence est le minimum des valeurs rgb pour toutes les images du mois à une heure indiquée.

On utilisera le logiciel Matlab pour ça, il a l’avantage d’avoir des toolboxes adaptées à cet usage, en plus d’être déjà maîtrisé par l’étudiant et l’enseignant.

On organisera ce papier en deux parties, une partie code et une partie résultats.

Une partie interprétation conclura le travail effectué.
